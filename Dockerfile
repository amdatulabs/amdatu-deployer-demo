FROM alpine:3.5

COPY ./amdatu-kubernetes-deployer-demo /go/bin/amdatu-kubernetes-deployer-demo

ENV PATH="/go/bin:$PATH"
WORKDIR /go/bin

ENTRYPOINT ["amdatu-kubernetes-deployer-demo"]