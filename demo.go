package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {

	http.HandleFunc("/healthy", func(w http.ResponseWriter, r *http.Request) {
		info := Health{Message: "Hello, Deployer!", Healthy: true}
		b, err := json.MarshalIndent(info, "", "  ")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprint(w, string(b))
	})

	http.HandleFunc("/unhealthy", func(w http.ResponseWriter, r *http.Request) {
		info := Health{Message: "Hello, Deployer!", Healthy: false}
		b, err := json.MarshalIndent(info, "", "  ")
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprint(w, string(b))
	})

	http.HandleFunc("/simple", func(w http.ResponseWriter, r *http.Request) {
	})

	http.HandleFunc("/error", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusInternalServerError)
	})

	http.HandleFunc("/delayed", func(w http.ResponseWriter, r *http.Request) {
		info := Health{Message: "Hello, Deployer, I'm slow!!!", Healthy: true}
		b, err := json.MarshalIndent(info, "", "  ")
		if err != nil {
			log.Fatal(err)
		}
		time.Sleep(40 * time.Second)
		fmt.Fprint(w, string(b))
	})

	log.Fatal(http.ListenAndServe(":9999", nil))

}

type Health struct {
	Message string `json:"message"`
	Healthy bool   `json:"healthy"`
}
