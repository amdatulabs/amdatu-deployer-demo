Amdatu Kubernetes Deployer Demo
===============================

This is a little helper project used by the Amdatu Kubernetes Deployer integration tests.
It provides 4 healthchecks on port 9999:

- /healthy: a healthy probe healthcheck
- /unhealthy: an unhealthy probe healthcheck
- /simple: a healthy simple healthcheck
- /error: an unhealthy simple healthcheck