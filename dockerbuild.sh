#!/usr/bin/env bash
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o deployer-demo_linux_amd64 .
docker build -t amdatu/amdatu-kubernetes-deployer-demo:test .
docker push amdatu/amdatu-kubernetes-deployer-demo:test